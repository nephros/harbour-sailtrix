import QtQuick 2.0
import Sailfish.Silica 1.0
import Messages 1.0
import Rooms 1.0

Page {
    id: page

    allowedOrientations: Orientation.All

    property string room_id
    property string room_name
    property Rooms rooms;

    Messages {
        id: backend
        rid: room_id
        onMessagesChanged: {
            if (list.atYEnd || !fullyLoaded) {
                list.positionViewAtEnd();
                fully_read();
            }
        }

        onNewMessage: {
            if (!list.atYEnd) {
                goToBottom.visible = true;
            } else {
                list.positionViewAtEnd();
                fully_read();
            }
        }

        onFullyLoadedChanged: {
            console.log("LOADED:" + fullyLoaded);
        }
    }

    SilicaListView {
        VerticalScrollDecorator {}

        id: list
        width: page.width
        spacing: Theme.paddingLarge

        anchors.top: page.top
        anchors.bottom: messageArea.top
        anchors.bottomMargin: Theme.paddingLarge

        header: PageHeader {
            title: room_name
        }

        model: backend.messages

        delegate: ListItem {
            height: chatBox.height + the_menu.height
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottomMargin: Theme.paddingMedium
            id: item

            menu: ContextMenu {
                id: the_menu;
                MenuItem {
                    text: "Reply"
                    onClicked: {
                        replyLabel.visible = true;
                        replyLabel.text = "Replying to " + (display_name ? display_name : user_id);
                        replyRect.visible = true;
                        list.anchors.bottom = replyRect.top;
                        backend.reply_mid = event_id;
                        backend.reply_orig_uid = user_id
                        backend.reply_orig_body = orig_body
                        backend.reply_orig_formatted = content
                    }
                    visible: event_id != undefined
                }

                MenuItem {
                    text: "Edit"
                    onClicked: {
                        replyLabel.visible = true;
                        replyLabel.text = "Editing message";
                        replyRect.visible = true;
                        list.anchors.bottom = replyRect.top;
                        messageArea.text = orig_body;
                        messageArea.focus = true;
                        backend.edit_event_id = event_id;
                    }

                    visible: is_self && !is_deleted

                }

                MenuItem {
                    text: "Delete"
                    onClicked: item.remorseDelete(function() { backend.redact(event_id);
                        texts.text = "<b>" + (display_name ? display_name : user_id) + "</b><br>🗑️ Message deleted"; } );
                    visible: is_self && !is_deleted
                }

            }

            Image {
                id: icon
                height: Theme.itemSizeExtraSmall
                width: height

                source: avatar ? avatar : "image://theme/icon-m-media-artists"
                anchors.right: is_self ? parent.right : undefined


                MouseArea {
                    anchors.fill: parent
                    onClicked: pageStack.push("User.qml", { user_id: user_id, display_name: display_name, avatar_url: avatar, display_ignore: !is_self, prev_page: pageStack.currentPage })
                }

                onStatusChanged: {
                    if (status == Image.Error) {
                        source = "image://theme/icon-m-media-artists";
                    }
                }
            }


            Rectangle {
                id: chatBox
                radius: 5
                anchors.right: is_self ? icon.left : parent.right
                anchors.left: !is_self ? icon.right : parent.left
                color: Theme.colorScheme === Theme.DarkOnLight 
                    ? Qt.darker(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                    : Qt.lighter(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                width: texts.contentWidth + Theme.dp(20)
                height: texts.contentHeight + Theme.dp(20)
                anchors.rightMargin: is_self ? 0 : Theme.itemSizeExtraSmall
                anchors.leftMargin: is_self ? Theme.itemSizeExtraSmall : 0

                Label {
                    id: texts
                    text: "<b>" + (display_name ? display_name : user_id) + "</b><br>" + content
                    color: highlighted ? Theme.highlightColor : Theme.primaryColor
                    // linkColor: Theme.primaryColor
                    wrapMode: "WrapAtWordBoundaryOrAnywhere"
                    leftPadding: Theme.dp(10)

                    width: page.width - icon.width - Theme.itemSizeExtraSmall - Theme.dp(4)

                    textFormat: "RichText"
                }

            }
        }

        onYChanged: {
            if (list.atYEnd) {
                backend.fully_read();
            }
        }
    }

    Rectangle {
        color: Theme.colorScheme === Theme.DarkOnLight
            ? Qt.darker(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            : Qt.lighter(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
        width: page.width
        height: replyLabel.height
        anchors.bottom: messageArea.top
        visible: false
        id: replyRect
        anchors.left: page.left
        anchors.right: page.right

        Label {
            id: replyLabel
            visible: false
            truncationMode: elide
            topPadding: Theme.paddingSmall
            bottomPadding: Theme.paddingSmall
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.rightMargin: Theme.horizontalPageMargin
            anchors.left: parent.left
            anchors.right: cancelButton.left
        }

        IconButton {
            id: cancelButton
            height: parent.height
            width: height
            icon.source: "image://theme/icon-splus-cancel"
            anchors.right: parent.right
            onClicked: {
                replyRect.visible = false;
                list.anchors.bottom = messageArea.bottom
                messageArea.text = "";
                backend.reply_mid = "";
                backend.edit_event_id = "";
            }
        }
    }

    TextArea {
        label: "Message"
        placeholderText: "Send message to room"
        id: messageArea
        anchors.bottom: page.bottom
        anchors.right: sendButton.left
        anchors.left: page.left
        placeholderColor: Theme.secondaryHighlightColor
        color: Theme.highlightColor
        background: Rectangle {
            width: messageArea.width
            height: messageArea.height
            color: Theme.colorScheme === Theme.DarkOnLight
                ? Qt.darker(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                : Qt.lighter(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
        }

    }


    Button {
        id: sendButton
        text: "Send"
        width: Theme.itemSizeMedium
        anchors.bottom: page.bottom
        anchors.right: page.right

        height: messageArea.height
        enabled: messageArea.text != ""

        onClicked: {
            backend.send(messageArea.text);
            messageArea.text = "";
            list.positionViewAtEnd()
            replyRect.visible = false;
            list.anchors.bottom = messageArea.top
            backend.reply_mid = "";
        }
    }

    Button {
        id: goToBottom
        text: "New Messages"
        width: page.width - (page.width * .20)
        anchors.bottom: page.bottom
        anchors.horizontalCenter: page.horizontalCenter
        anchors.bottomMargin: Theme.dp(75);
        opacity: 1
        visible: false;
        backgroundColor: Theme.highlightBackgroundColor
        color: Theme.highlightColor
        onClicked: {
            list.positionViewAtEnd();
            visible = false;
        }
    }

    PageBusyIndicator {
        running: !backend.fullyLoaded
    }

    Component.onCompleted: backend.load()
}
