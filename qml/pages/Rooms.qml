import QtQuick 2.0
import Sailfish.Silica 1.0
import Rooms 1.0

Page {
    id: page

    allowedOrientations: Orientation.All

    Rooms {
        id: rooms
    }


    SilicaListView {
        VerticalScrollDecorator {}

        id: list
        width: page.width
        height: page.height;
        spacing: Theme.paddingLarge


        header: PageHeader {
            title: "Rooms and Messages"
        }

        PullDownMenu {
            MenuItem {
                text: "About"
                onClicked: pageStack.push("Credits.qml");
            }

            MenuItem {
                text: "Settings"
                onClicked: pageStack.push("Settings.qml");
            }
        }

        model: rooms.rooms
        delegate: ListItem {
            id: item

            anchors.left: parent.left
            anchors.right: parent.right

            menu: ContextMenu {
                MenuItem {
                    text: "Leave"
                    onClicked: item.remorseDelete(function() { rooms.leave(rid); });
                }
            }


            onClicked: {
                pageStack.push("Messages.qml", { room_name: name, room_id: rid});
            }


            Image {
                id: img
                source: avatar ? avatar : ("image://theme/icon-m-chat?" + (pressed ? Theme.highlightColor : Theme.primaryColor))
                height: roomName.height + preview.height
                width: height

                onSourceChanged: console.log(source);
            }
            Label {
                id: roomName
                text: unread > 0 ? "(" + unread + ") " + name : name
                anchors.left: img.right
                anchors.leftMargin: Theme.horizontalPageMargin
                truncationMode: TruncationMode.Fade
                font.pixelSize: Theme.fontSizeMedium
                width: page.width - img.width - Theme.horizontalPageMargin - Theme.horizontalPageMargin
                font.bold: unread > 0
                color: Theme.primaryColor
            }
            Label {
                id: preview
                text: latestMessage
                anchors.top: roomName.bottom
                anchors.left: img.right
                anchors.right: list.right
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin

                font.pixelSize: Theme.fontSizeExtraSmall
                truncationMode: TruncationMode.Fade
                width: page.width - img.width - Theme.horizontalPageMargin - Theme.horizontalPageMargin
                color: Theme.primaryColor
            }
        }
    }

    PageBusyIndicator {
        running: !rooms.done
    }

    Component.onCompleted: rooms.load();

    onStatusChanged: {
        console.log(status);
        if (status == PageStatus.Inactive) {
            rooms.pause();
        } else if (status == PageStatus.Active) {
            if (rooms.loaded())
                rooms.resume();
            else
                rooms.load();
        }
    }
}
