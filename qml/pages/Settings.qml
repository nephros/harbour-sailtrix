import QtQuick 2.0
import Sailfish.Silica 1.0
import SettingsBackend 1.0

Page {
    id: page

    allowedOrientations: Orientation.All

    property bool running;

    SettingsBackend {
        id: backend;
        onDone: {
            pageStack.replaceAbove(null, "Rooms.qml");
           }
        onConfigClearDone: {
            pageStack.replaceAbove(null, "Start.qml");
        }
    }

    Column {
        id: column
        width: page.width
        height: page.height;
        spacing: Theme.paddingLarge

        PageHeader {
            title: "Settings"
        }

        Button {
            text: "Clear cache";
            onClicked: Remorse.popupAction(root, "Cleared cache", function() { backend.clear_cache() });
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            text: "Logout"
            onClicked: {
                Remorse.popupAction(root, "Logging out", function() {
                    running = true;
                    backend.clear_config();
                });
            }
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
        }


        BusyIndicator {
            size: BusyIndicatorSize.Large
            anchors.horizontalCenter: parent.horizontalCenter
            id: indicator
            running: running
        }
    }
}
