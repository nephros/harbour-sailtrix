#include <QtQuick>

#include <sailfishapp.h>
#include <QStandardPaths>

#include "loginbridge.h"
#include "rooms.h"
#include "messages.h"
#include "settingsbackend.h"
#include "messages.h"
#include "userbackend.h"
#include "olm/olm.h"

int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/Sailtrix.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
    //
    // To display the view, call "show()" (will show fullscreen on device).


    qmlRegisterType<LoginBridge>("LoginBridge", 1, 0, "LoginBridge");
    qmlRegisterType<Rooms>("Rooms", 1, 0, "Rooms");
    // qmlRegisterType<Messages>("Messages", 1, 0, "Messages");
    qmlRegisterType<SettingsBackend>("SettingsBackend", 1, 0, "SettingsBackend");
    qmlRegisterType<UserBackend> ("UserBackend", 1, 0, "UserBackend");
    qmlRegisterType<Messages>("Messages", 1, 0, "Messages");
    qRegisterMetaType<RoomsModel*>("RoomsModel*");
    qRegisterMetaType<MessagesModel*>("MessagesModel*");


    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
    QScopedPointer<QQuickView> view(SailfishApp::createView());


    qDebug() << "Creating file...";

    QString configFileName(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/sailtrix.config.json");
    QFile file(configFileName);

    if (file.exists()) {
        qDebug() << "Before setting view...";
        view->setSource(SailfishApp::pathTo(QString("qml/logged-in.qml")));
        qDebug() << "After setting view...";

        view->show();
    } else {
        view->setSource(SailfishApp::pathTo(QString("qml/harbour-sailtrix.qml")));
        view->show();
    }

    qDebug() << "Starting sailtrix...";

    return app->exec();
}
