#ifndef MESSAGES_H
#define MESSAGES_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QQueue>
#include "message.h"
#include "messagesmodel.h"
#include <QJsonObject>
#include <olm/olm.h>

class Messages : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MessagesModel* messages READ messages WRITE setMessages NOTIFY messagesChanged )
    Q_PROPERTY(QString rid READ rid WRITE setRid NOTIFY idChanged )
    Q_PROPERTY(bool typing READ typing WRITE setTyping)
    Q_PROPERTY(bool fullyLoaded READ fully_loaded WRITE setFullyLoaded NOTIFY loaded)
    Q_PROPERTY(QString reply_mid READ reply_mid WRITE setReplyMid)
    Q_PROPERTY(QString reply_orig_uid READ reply_orig_uid WRITE setReplyOrigUid)
    Q_PROPERTY(QString reply_orig_body READ reply_orig_body WRITE setReplyOrigBody)
    Q_PROPERTY(QString reply_orig_formatted READ reply_orig_formatted WRITE setReplyOrigFormatted)
    Q_PROPERTY(QString edit_event_id READ edit_event_id WRITE setEditEventId)

public:
    Messages();
    ~Messages();
    MessagesModel* messages();
    QString rid();
    void setRid(QString id);
    void setMessages(MessagesModel* model);
    Q_INVOKABLE bool load();
    bool typing();
    void setTyping(bool typing);
    bool fully_loaded();
    void setFullyLoaded(bool fullyLoaded);
    QString reply_mid();
    void setReplyMid(QString reply_mid);
    QString reply_orig_uid();
    void setReplyOrigUid(QString reply_orig_uid);
    QString reply_orig_body();
    void setReplyOrigBody(QString reply_orig_body);
    QString reply_orig_formatted();
    void setReplyOrigFormatted(QString reply_orig_formatted);
    QString edit_event_id();
    void setEditEventId(QString edit_event_id);
    Q_INVOKABLE void send(QString message);
    Q_INVOKABLE void redact(QString event_id);
    Q_INVOKABLE void fully_read();
signals:
    void messagesChanged();
    void idChanged();
    void newMessage();
    void loaded();
private:
    QString m_id;
    MessagesModel* m_messages;
    QString m_hs_url;
    QString m_access_token;
    QString m_user_id;
    QString m_next_batch;
    QNetworkAccessManager* getter;
    QNetworkAccessManager* avatar_getter;
    QNetworkAccessManager* message_sender;
    QNetworkAccessManager* redactor;
    QNetworkAccessManager* device_getter;
    QNetworkAccessManager* otk_claimer;
    QNetworkAccessManager* rk_sender;
    QNetworkAccessManager* member_getter;
    QNetworkAccessManager* marker_setter;
    QNetworkAccessManager* filter_maker;
    QJsonObject* users;
    QJsonObject* json_messages;
    QJsonObject* olm_sessions;
    QJsonObject* megolm_sessions;
    QJsonObject* devices;
    QQueue<QJsonObject>* messages_queue;
    QQueue<QString>* encrypted_messages_queue;
    QList<QString>* m_local_event_ids;
    bool m_sending;
    bool m_typing;
    bool m_fully_loaded;
    QString m_reply_mid;
    QString m_reply_orig_uid;
    QString m_reply_orig_body;
    QString m_reply_orig_formatted;
    QString m_edit_event_id;
    bool m_encrypted;
    bool has_all_devices;
    bool has_new_members;
    QString m_encryption_type;
    int m_rotation_ms;
    int m_rotation_msg;
    QString m_curve25519_id;
    QString m_ed25519_key;
    OlmAccount* account;
    OlmOutboundGroupSession* outbound_megolm_session;
    unsigned int message_index;
    int m_needed_otk;
    QJsonObject* send_to_device_room_key;
    QJsonObject room_key_to_encrypt;
    QString m_outbound_session_id;
    QString m_device_id;
    int m_messages_sent;
    QJsonObject* seen_message_indices;
    QString last_sent_fully_read;
    bool get_immediately;
    QString m_filter_id;
private slots:
    void processMessages(QNetworkReply* reply);
    void processAvatar(QNetworkReply* reply);
    void processSentMessage(QNetworkReply* reply);
    void processDevices(QNetworkReply* reply);
    void processNewOneTimeKeys(QNetworkReply* reply);
    void afterSendRoomKeys(QNetworkReply* reply);
    void processMembers(QNetworkReply* reply);
    void afterFullyRead(QNetworkReply* reply);
    void processFilter(QNetworkReply* reply);
    void afterRedact(QNetworkReply* reply);
private:
    QUrl getUrl();
    void write_cache();
    void read_cache();
    void process_remote_echo(QString event_id, QString txn_id);
    bool process_message(QJsonObject message);
    bool process_state_event(QJsonObject event_obj, QJsonArray* new_users);
    bool create_outbound_megolm_session();
    void start_olm_session(QString target_user,  QString target_device);
    void send_room_key();
    void send_messages_in_queue();
    void remove_devices_of(QString user_id);
    QString getFilter();
};

#endif // MESSAGES_H
