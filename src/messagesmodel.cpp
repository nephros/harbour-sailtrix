#include "messagesmodel.h"

QHash<int, QByteArray> MessagesModel::roleNames() const {
    QHash<int, QByteArray> list;
    list[user_id] = "user_id";
    list[display_name] = "display_name";
    list[avatar] = "avatar";
    list[avatar_mxc] = "avatar_mxc";
    list[content] = "content";
    list[is_self] = "is_self";
    list[event_id] = "event_id";
    list[txn_id] = "txn_id";
    list[orig_body] = "orig_body";
    list[is_deleted] = "is_deleted";

    return list;
}
