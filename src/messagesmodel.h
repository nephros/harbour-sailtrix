#ifndef MESSAGESMODEL_H
#define MESSAGESMODEL_H
#include <QStandardItemModel>

class MessagesModel : public QStandardItemModel
{
public:
    enum Role {
        user_id=Qt::UserRole,
        display_name,
        content,
        avatar_mxc,
        avatar,
        is_self,
        event_id,
        txn_id,
        orig_body,
        is_deleted
    };

    explicit MessagesModel(QObject * parent = 0): QStandardItemModel(parent) {}

    explicit MessagesModel( int rows, int columns, QObject * parent = 0 ): QStandardItemModel(rows, columns, parent) {}

    QHash<int, QByteArray> roleNames() const;
};

#endif // MESSAGESMODEL_H
