#include "roomsmodel.h"

QHash<int, QByteArray> RoomsModel::roleNames() const {
    QHash<int, QByteArray> list;
    list[name] = "name";
    list[icon] = "avatar";
    list[firstMessage] = "latestMessage";
    list[unreadMessages] = "unread";
    list[roomId] = "rid";

    return list;
}
