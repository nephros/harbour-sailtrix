#ifndef ROOMSMODEL_H
#define ROOMSMODEL_H
#include <QStandardItemModel>


class RoomsModel : public QStandardItemModel
{
public:
    enum Role {
        name=Qt::UserRole,
        icon,
        firstMessage,
        unreadMessages,
        mxcUrl,
        roomId,
    };

    explicit RoomsModel(QObject * parent = 0): QStandardItemModel(parent) {}

    explicit RoomsModel( int rows, int columns, QObject * parent = 0 ): QStandardItemModel(rows, columns, parent) {}

    QHash<int, QByteArray> roleNames() const;
};

#endif // ROOMSMODEL_H
