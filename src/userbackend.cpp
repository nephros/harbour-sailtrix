#include "userbackend.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStandardPaths>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonArray>

UserBackend::UserBackend() : blocker { new QNetworkAccessManager(this) }, room_creator { new QNetworkAccessManager(this) }, account_data_sender { new QNetworkAccessManager(this) }, is_done { false } {
    connect(blocker, &QNetworkAccessManager::finished, this, &UserBackend::block_done);
    connect(room_creator, &QNetworkAccessManager::finished, this, &UserBackend::create_room_done);
    connect(account_data_sender, &QNetworkAccessManager::finished, this, &UserBackend::m_direct_sent);

    QFile config (QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/sailtrix.config.json");

    if (config.open(QFile::ReadOnly)) {
        QJsonDocument config_doc = QJsonDocument::fromJson(config.readAll());

        if (config_doc.object().value("home_server").toString() == ""
                || config_doc.object().value("access_token").toString() == "") return;

        m_hs_url = config_doc.object().value("home_server").toString();
        m_access_token = config_doc.object().value("access_token").toString();
        m_user_id = config_doc.object().value("user_id").toString();
        m_device_id = config_doc.object().value("device_id").toString();
   }
}

bool UserBackend::done() {
    return is_done;
}

void UserBackend::change_done(bool n) {
    qFatal("Illegal to change");
    n = false; // Suppresses warning
}

void UserBackend::block(QString to_block) {
    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/user/" + m_user_id + "/account_data/m.ignored_user_list");
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

    QFile existing_blocklist(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/blocked_users.json");
    QJsonObject to_send;
    QJsonObject ignored_users;

    if (existing_blocklist.open(QFile::ReadOnly)) {
        QJsonDocument doc = QJsonDocument::fromJson(existing_blocklist.readAll());
        ignored_users = doc.object();
        existing_blocklist.close();
    }

    ignored_users.insert(to_block, QJsonObject());

    if (existing_blocklist.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        doc.setObject(ignored_users);
        existing_blocklist.write(doc.toJson());
        existing_blocklist.close();
    }

    to_send.insert("ignored_users", ignored_users);

    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    blocker->put(req, QJsonDocument(to_send).toJson());
}

void UserBackend::block_done(QNetworkReply* reply) {
    qDebug() << reply->error();
    is_done = true;
    emit done_changed();
}

void UserBackend::start_dm(QString to_dm) {
    QJsonObject to_send;
    to_send.insert("preset", "trusted_private_chat");

    QJsonArray to_invite;
    to_invite.append(to_dm);

    to_send.insert("invite", to_invite);
    to_send.insert("is_direct", true);
    to_send.insert("visibility", "private");

    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/createRoom");
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

    m_user_to_dm = to_dm;

    room_creator->post(req, QJsonDocument(to_send).toJson());

}

bool UserBackend::is_blocked(QString user) {
    QFile existing_blocklist(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/blocked_users.json");
    if (existing_blocklist.open(QFile::ReadOnly)) {
        QJsonDocument doc = QJsonDocument::fromJson(existing_blocklist.readAll());
        if (doc.object().contains(user)) {
            return true;
        }
    }

    return false;
}
void UserBackend::unblock(QString to_unblock) {
    QNetworkRequest req(m_hs_url + "/_matrix/client/r0/user/" + m_user_id + "/account_data/m.ignored_user_list");
    req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

    QFile existing_blocklist(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/blocked_users.json");
    QJsonObject to_send;
    QJsonObject ignored_users;

    if (existing_blocklist.open(QFile::ReadOnly)) {
        QJsonDocument doc = QJsonDocument::fromJson(existing_blocklist.readAll());
        ignored_users = doc.object();
        existing_blocklist.close();
    }

    ignored_users.remove(to_unblock);

    if (existing_blocklist.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        doc.setObject(ignored_users);
        existing_blocklist.write(doc.toJson());
        existing_blocklist.close();
    }

    to_send.insert("ignored_users", ignored_users);

    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    blocker->put(req, QJsonDocument(to_send).toJson());
}

void UserBackend::create_room_done(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        m_new_room_id = doc.object().value("room_id").toString();


        QJsonObject dms;
        QFile direct_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/direct.json");
        if (direct_file.open(QFile::ReadOnly)) {
            dms = QJsonDocument::fromJson(direct_file.readAll()).object();
            direct_file.close();
        }

        QJsonArray arr = dms.value(m_user_to_dm).toArray();
        arr.append(m_new_room_id);

        dms.insert(m_user_to_dm, arr);

        if (direct_file.open(QFile::WriteOnly)) {
            direct_file.write(QJsonDocument(dms).toJson());
            direct_file.close();
        }

        QNetworkRequest req(m_hs_url + "/_matrix/client/r0/user/" + m_user_id + "/account_data/m.direct");
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
        req.setRawHeader(QByteArray("Authorization"), QString("Bearer " + m_access_token).toUtf8());

        account_data_sender->put(req, QJsonDocument(dms).toJson());
    }
}

QString UserBackend::new_room_id() {
    return m_new_room_id;
}

void UserBackend::change_room_id(QString n) {
    return;
}

void UserBackend::m_direct_sent(QNetworkReply* reply) {
    emit room_id_changed();
}
