#ifndef USERBACKEND_H
#define USERBACKEND_H

#include <QObject>
#include <QNetworkAccessManager>

class UserBackend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool is_done READ done WRITE change_done NOTIFY done_changed)
    Q_PROPERTY(QString new_room_id READ new_room_id WRITE change_room_id NOTIFY room_id_changed)

public:
    UserBackend();
    Q_INVOKABLE void block(QString to_block);
    Q_INVOKABLE void start_dm(QString to_dm);
    Q_INVOKABLE bool is_blocked(QString user);
    Q_INVOKABLE void unblock(QString to_unblock);
    bool done();
    bool blocked();
    void change_done(bool n);
    QString new_room_id();
    void change_room_id(QString n);

signals:
    void done_changed();
    void room_id_changed();
private:
        QString m_uid;
        QString m_hs_url;
        QString m_user_id;
        QString m_access_token;
        QString m_device_id;
        QString m_user_to_dm;
        QNetworkAccessManager* blocker;
        QNetworkAccessManager* room_creator;
        QNetworkAccessManager* account_data_sender;
        bool is_done;
        QString m_new_room_id;
private slots:
        void block_done(QNetworkReply* reply);
        void create_room_done(QNetworkReply* reply);
        void m_direct_sent(QNetworkReply* reply);

};

#endif // USERBACKEND_H
